---
title: Restricted environments
date: 2024-12-05
quickplace_restricted:
  - var: atani.quickplace_-_Strip_Club_dressing_room.8
    title: Strip Club dressing room
  - var: atani.quickplace_-_Teen_boys_bedroom.8
    title: Teen boy Bedroom*
---

#### These subscenes only work on prepared scenes if you follow the instructions in the main _quickplace_ page.

----

### No environment included

As the environment used is paid or restricted in some way, I cannot include it and you need to have it on your disk to be able to use the _quickplace_ subscene.

If you have the environment shown in the picture, the subscene expects it to be in/as:
- _Custom/Assets/Environments/_**title**_.assetbundle_

**Note**: titles with a asterisk (*) are _.scene_ instead of _.assetbundle_

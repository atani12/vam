---
title: Free Hub environments
date: 2025-01-10
quickplace:
  - var: atani.quickplace_-_Index.4
    title: Index
  - var: atani.quickplace_-_Apartment_-_DarkFantasy.8
    title: Apartment
  - var: atani.quickplace_-_Apartment_-_Nial.8
    title: Apartment
  - var: atani.quickplace_-_Apartment_-_VamTastic.8
    title: Apartment
  - var: atani.quickplace_-_Beach_day_-_VamTastic.8
    title: Beach - day
  - var: atani.quickplace_-_Beach_night_-_Romolas.8
    title: Beach - night
  - var: atani.quickplace_-_Cabin_-_TacoCat.8
    title: Cabin
  - var: atani.quickplace_-_Cafe_-_VamTastic.8
    title: Café
  - var: atani.quickplace_-_Cave.2
    title: Cave
  - var: atani.quickplace_-_Classroom_-_VaMSleSar.8
    title: Classroom
  - var: atani.quickplace_-_CyberPunkroom_-_Chase81.8
    title: CyberPunk room
  - var: atani.quickplace_-_Diner_-_vamX.8
    title: Diner
  - var: atani.quickplace_-_Estate_-_VamTastic.8
    title: Estate
  - var: atani.quickplace_-_Fallout_house_-_Orpheaned.1
    title: Fallout house
  - var: atani.quickplace_-_Fireplace_bedroom_-_VamTastic.8
    title: Fireplace 
  - var: atani.quickplace_-_Forest_-_MR.1
    title: Forest day/night
  - var: atani.quickplace_-_Game_shop_-_VamTastic.8
    title: Game shop
  - var: atani.quickplace_-_Girl_Bedroom_-_Xnop73.8
    title: Girl Bedroom
  - var: atani.quickplace_-_Gwin_Cerbin_-_Norm.8
    title: Gwin Cerbin
  - var: atani.quickplace_-_Hot-tub.8
    title: Hot-tub
  - var: atani.quickplace_-_Kame_house_-_SuperSamoth.8
    title: Kame house
  - var: atani.quickplace_-_Locker_room_-_Abubu.8
    title: Locker room
  - var: atani.quickplace_-_Motel_room_-_Chase81.8
    title: Motel room
  - var: atani.quickplace_-_Office_-_Nial.8
    title: Office
  - var: atani.quickplace_-_Old_bedroom.8
    title: Old bedroom
  - var: atani.quickplace_-_Police_interview_room_-_senorcafe.8
    title: Police interview room
  - var: atani.quickplace_-_Porn_Studio_-_Molmark.8
    title: Porn Studio
  - var: atani.quickplace_-_Primitive_tent_-_Romolas.8
    title: Primitive tent
  - var: atani.quickplace_-_Skyrim_Cottage_-_VamTastic.8
    title: Skyrim Cottage
  - var: atani.quickplace_-_Spaceship_-_Orpheaned.1
    title: Spaceship
---

#### These subscenes only work on prepared scenes if you follow the instructions in the main _quickplace_ page.

## _quickdress_ Index

This is a special _quickplace_ subscene that has a image UI to easily load all my currently shared _quickplace_ VARs. You're also able to change _quickplace_ types to load on suitable scenes. Naturally, you can only load _quickplace_ subscenes with it if you already have the suitable _quickplace_ VAR in your system.

Keep in mind that the **R** button is for the _restricted_ _quickplace_ subscenes that do not use Hub hosted environments. Read more about it in the category page.

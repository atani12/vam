---
title: anatomy of a VAR
date: 2023-06-28
summary: it's not just a zip file
thumbnail: anatomy_var
mega:
hub: https://hub.virtamate.com/resources/anatomy-of-a-var.36093/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/262/262662-39de25277dd8682cc8a849e1e5a8dbe8.data)


### This guide means to show what makes a VAR a VAR

A VAR is the prefered way to share content between VaM users, with the Hub hosting thousands of them as I write this guide. Yet, this file only exists from VaM version 1.20, if not mistaken, and before it were the Dark Ages of VaM content sharing. I joke and exagerate of course, but before VARs sharing VaM content was not as easy or controllable as is with it, but that's another story.

---
title: quickmusic
date: 2025-03-03
layout: layouts/quickmusic.njk
thumbnail: quickmusic/quickmusic
folder: quickmusic
quickmusic:
  - url: https://mega.nz/file/ewkBDTAQ#upNQ0isDhzzToU9_Obp7hVEG9hRtfpOwt8uV3aRqtX0
    image: quickmusic-index
  - url: https://mega.nz/file/jgNhTYRQ#Iq_rfqt7WMJ5Lo2gS3fZ4PKefHMaEpJVBE_1CTObLpY
    image: addams_wednesday
  - url: https://mega.nz/file/3x1VWKyL#R9dEVOUvIUZkfQwzytw_yQWj8rBa7hvmdnJQsHsVPnw
    image: cobra_kai
  - url: https://mega.nz/file/ipd2XJxD#qbf3RuEVc2uJIDjWypXJPHvoi9Z3Wqoj9_S7-i0cCUw
    image: got
  - url: https://mega.nz/file/elN3gQbK#J1MO-TxBvD-2Hx7nkpcGmOu81zlt_YDRVwD-zIGYT2U
    image: harry_potter
  - url: https://mega.nz/file/3gFEFIzT#WC0VNFhLqA-Hvy9cIlfjRySK6oqlIC9YoLD0zixFQ0o
    image: lotr
  - url: https://mega.nz/file/29cC1RjR#WKRS1OfJZtxygqiDRtkYgriw9f-OTcWueHXBHK_FTjw
    image: marvel
  - url: https://mega.nz/file/CkFgzBxT#F-kG3YFWbWV8G-K9TaVgYATFmtDcFKJvbtzrAPO1h7g
    image: pirates_caribbean
  - url: https://mega.nz/file/C1ND0BoR#oyyq_0vAERik3fOPuRyZOSKbrJLubWZo5xiUSeSCLms
    image: star_wars
  - url: https://mega.nz/file/C5VT0bjY#qcmv4XLG5m2wS11ABr9lVALlZkmoKRYrBc1cPOLHSwY
    image: tron
  - url: https://mega.nz/file/Ps0SgA7L#WoZ-pmY7swAcjmrcyVI6Pwga_SsT5czBvkXmHCD7ytU
    image: witcher
  - url: https://mega.nz/file/75dVmaIY#GqpKo_wjIoAAv0-xgmaOZhJ96spc8s1y0uJMRlqixNc
    image: beat
  - url: https://mega.nz/file/zlsXBLjQ#piYV5Ma1dPah2GNgOvX6X3xe_CN7oKYKrXt32aliY-4
    image: beat2
  - url: https://mega.nz/file/zldnDYJJ#ks65R5_aHZRSYQAoCR0GnMckOMHZZi2uuCA_uKrjWxM
    image: college
  - url: https://mega.nz/file/Wkky2TKa#7y6ahGm_6l6JsuJyPoW9yMkDcavxZMqwpR8C0sOWmiQ
    image: soft
  - url: https://mega.nz/file/T10BmThJ#Vs7Zcz2j0Z89-kx5ZPonX7HtVbZLwWbP7X-rGsdm39U
    image: techno
---

### Music playlists for NEXT and 2023 Template scenes

1. Load the AudioSource preset included in the VAR on the _quickmusic_ atom
2. Play and control music from the _Audio_ UI

#### Combine with _quickdress_ and character presets for best results

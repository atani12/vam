---
title: Doric
date: 2023-09-10
summary:  For faun fun
thumbnail: doric/doric
images:
  - doric/doric1
  - doric/doric2
hub: 
mega: https://mega.nz/file/ztdHDRKR#hHASNU6TOgt_RZQoG-nU8KUINPuOOZhUXyk2kItFO0g
---

## About the look
This appearance combines vecterror's SoLi with Syrinxo's Tauna to create a faun version of the character.


### Appearance preset only, no scene included
The VAR only has a appearance preset, it won't show up in the scenes browser.


### Non-hub assets used:
- vecterror.SoLi


### Credits:
- Cloudcover
- Jachu
- paledriver
- Spacedog
- Syrinxo
- vecterror

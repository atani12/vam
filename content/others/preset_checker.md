---
title: Preset VAR checker
date: 2023-09-08
summary: lists missing references between your presets and your VARs
thumbnail: preset_checker
mega:
hub: https://hub.virtamate.com/resources/preset-var-checker.38560/
---

![banner](https://1387905758.rsc.cdn77.org/internal_data/attachments/285/285516-2e21fbd35a736fffc7e251835b9c2d23.data)


### This resource is a python 3 script that reads the VAR references in your preset files, compares them with your VARs, and creates a list with the differences.​


#### Latest version features:
- Appearance presets​
- Clothing presets​
- Hair presets​
- UI for selecting what presets to check​


#### Why would I want to use this script?
- If you're one of those people (like me) that periodically deletes VARs to keep VaM light, and then later on realises a few presets are now broken and look hideous, then this script may be useful to you.
- If you're a nut that keeps only the latest version of VARs (also like me) and want to have all presets referencing that latest version for... hummm... well, no practical reason but it's super clean and neat that way, then this script may be useful to you too.
- Because you can and need no reason to do what you want

---
title: Alpha textures for clothes
date: 2023-01-19
summary: Clothing with improved "accessibility"
thumbnail: alphas
mega:
hub: https://hub.virtamate.com/resources/alphas.20785/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/201/201304-0ba0df0a0b2b7a584d42d3fd4d18dd50.data)


The textures were created by me, it's free content, and suit the clothes listed. I do not share anything else besides the textures I created.
Please see the instructions below on how to use these textures.

## Instructions:
- On the respective clothing options add it to Textures > _AlphaTex
- If it's a decal it goes in Textures > _DecalTex
- Save it as a clothing preset if you want to have it always available


## Available textures as "VAR: file"
- See the textures list in the Hub asset description.

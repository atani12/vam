---
title: quickplace
date: 2024-03-17
summary:  Quickly load new environments
thumbnail: quickplace
hub: https://hub.virtamate.com/resources/quickplace-subscenes.45008/
---

![guide cover](https://c754e21d1d.mjedge.net/internal_data/attachments/418/418321-7533dead61863472ab844d473d6e9812.data)

## What are _quickplace_ subscenes?

They are subscenes with environments that you can quickly load in a compatible scene.

The goal is to have a modular and scalable approach to multiple environments in compatible scenes. This allows you to reuse a scene with a different experience without having to create multiple copies or include the environments in the scene.

### Where can I get _quickplace_ subscenes?

There's a dedicated section of this website to these subscenes, see the UI on top.

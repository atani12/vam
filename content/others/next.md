---
title: How to use the NEXT template scene
date: 2024-08-02
summary: Build a scene in 60 seconds with NEXT
thumbnail: next/next_guide
mega:
hub: https://hub.virtamate.com/resources/how-to-use-the-next-template-scene.49238/
---

![guide cover](https://c754e21d1d.mjedge.net/internal_data/attachments/396/396324-e43f109d56d1c200f103f5b77312bf01.data)

## This guide applies to this scene:

- [NEXT](https://atani12.gitlab.io/vam/scenes/next/)

It will show how you can best use the NEXT template scene features to have a complete scene in under 60 seconds.
And if you get bored with the environment, music, appearances or clothes, change them all again just as quickly, as many times as you want.

Read the guide in the Hub to learn more and play endless scenes with NEXT.

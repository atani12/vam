---
title: Transparency for background persons
date: 2023-10-22
summary: How to fake more people in a scene
thumbnail: transparency_for_background_persons
mega:
hub: https://hub.virtamate.com/resources/transparency-for-background-persons.40027/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/298/298922-4855fee106a2ab967f5b77323e3ae58e.data)


###  How to fake more people in a scene

#### This guide will focus on the perspective method to set up a scalable workflow for generating transparent background photos of person atoms.

What this means is that with the tools provided, you can:
- take multiple-angled photos of a posed person
- quickly remove the background
- import into a scene a set of ImageTransparentPanels to show the photos you made
- tweak them to blend in the scene's environment and POV

**POV in perspective**: the photos above are limited to a angle range based on the point of view (POV) of the camera. As you can expect, the depth perception exploited here with a 2D person photo doesn't work with a 360º, more like up to 30-45º away the focus point. As such, this method is best employed with possession or in a limited movement location.

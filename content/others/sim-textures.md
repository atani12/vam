---
title: A guide for Sim textures
date: 2023-01-14
summary:  The clothes must flow
thumbnail: sim-textures
hub: https://hub.virtamate.com/resources/a-guide-for-sim-textures.28569/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/199/199575-6d54414eb287937f18237db6619046f9.data)


## What is a Sim texture?
A Sim texture is a image file that tells VaM how stretchy a clothes item can be. ⚠ this is how I picture it, could be very wrong ⚠

This information is provided by the red channel in the image. A spot with a red value of 255 (full red) will have no stretching, while a red value of 0 will have maximum stretching.

If you've opened the VARs of some clothes, you may have seen some textures painted red and blue (or black). Those were the sim textures for the clothing item.
As mentioned, only the red channel matters for Sim, the green and blue channels are ignored. As a background for the red areas you want to add, it's common to use blue (255) instead of black because it's more visible, but the result is the same sim-wise.


## Instructions:
- See the guide's description in the Hub


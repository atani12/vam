---
title: Doll boxes
date: 2024-09-08
summary:  Boxes for hyper-realistic sex dolls
thumbnail: doll_boxes/doll_boxes
images:
  - doll_boxes/barbie
  - doll_boxes/black_widow
  - doll_boxes/hermione
  - doll_boxes/lotr
  - doll_boxes/star_trek
  - doll_boxes/star_wars
  - doll_boxes/wednesday
  - doll_boxes/witcher
hub: https://hub.virtamate.com/resources/make-a-sex-doll-box.48831/
mega: 
---
## VaM's hyper-realistic sex dolls need boxes too

People say that sometimes unboxing is more fun than the actual contents of the box. I'm not so sure that is the case here - pun intended - but you be the judge.


## Subscenes
These boxes are subscenes:
1. Add a subscene atom in the midsection of the person and at ground height, more or less;
2. Bring the subscene atom forward a little from the person, about a palm's length - the front panel is aligned vertically with the subscene atom - for more realistic box results;
3. Load a subscene from the DollBox presets available.


## About the doll boxes
After some experimenting, I decided to go with _ImagePanelTransparentEmissive_ atoms to make the box. This seems to be a reasonable option regarding lighting, ease of use, and other factors, compared to for example a _ISCube_ atom.

### Make your own
Besides the subscenes shown below in the gallery, you also have a _Base_ subscene, just a box without images. To make it even easier, I also include **templates** to make your box sides with the right sizes.

#### Templates
There's **.xcf** and **.png** files in the VAR's /Custom/Images/Doll boxes/ folder. The .xcf files are GIMP files you can use to make your fancy cover. Alternatively, the .png files can be used for the same purpose in any image editor.

If you have only basic image editing knowledge this is a great opportunity to improve them. You can go a long way with just a bit of training.

More detailed instructions can be found in the Hub's resource page, follow the link provided

----
## v6: 2024-09-08
- Wednesday box
- covers for UI

## v5: 2024-08-04
- higher render queue value on images to prevent bleed with CUA environments

## v4: 2024-07-21
- Witcher box

## v3: 2024-07-20
- LOTR box
- Better subscene photos 

## v2: 2024-07-18
- Star Trek and Star Wars boxes
- Hub link for a full guide

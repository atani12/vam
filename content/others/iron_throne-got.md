---
title: Iron Throne GOT
date: 2023-11-22
summary:  Reserved to the King/Queen of the Andals, the Rhoynar, and the First Men
thumbnail: iron_throne-got/iron_throne-got
images:
  - iron_throne-got/got
  - iron_throne-got/front
  - iron_throne-got/side
hub: https://hub.virtamate.com/resources/iron-throne-got.41078/
---

## About the asset
This VAR contains a assetbundle with the Iron Throne shown above. If you don't know how to load a assetbundle in VaM go see the tutorials.

The environment and persons are not included, neither is a scene.

### Notes
Use it with a scale of 1.5.

### Credits:
BlenderB_Rodriguez
JustLookinForNothin
MacGruber
Miki
Roac
Sharr
Sirap
The Mangle (sketchfab asset)
VaMChan
VamTastic
Vecterror
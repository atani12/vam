---
title: quickdress
date: 2023-04-17
summary:  Quickly load and control clothing presets
thumbnail: quickdress
hub: https://hub.virtamate.com/resources/quickdress-quickly-load-and-control-clothing-presets.32469/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/230/230963-b0ca1ebd50b0aa35696f17b71d9ed68b.data)


## Notes:
With this you can add to new and existing scenes: 
- Load clothing preset with a control UI
- Clothing animations possibilities
- Demo scene included


## Presets and instructions:
You can find my _quickdress_ presets and instructions in the Clothing category

---
title: Aida
date: 2024-08-05
summary:  AI girl to VaM girl
thumbnail: aida/aida
images:
  - aida/aida_profile
  - aida/portrait
  - aida/side
hub: https://hub.virtamate.com/resources/aida.33306/
---

## The goal
![AI girl](https://1387905758.rsc.cdn77.org/internal_data/attachments/238/238176-5d4b08512d350277abe58219107604dc.data)


### Appearance preset only, no scene included
The VAR only has a appearance preset, it won't show up in the scenes browser.


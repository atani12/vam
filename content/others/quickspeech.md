---
title: quickspeech
date: 2023-04-04
summary:  Quickly load and switch random audio speech
thumbnail: quickspeech
hub: https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/
---

![guide cover](https://c754e21d1d.mjedge.net/internal_data/attachments/418/418324-661141929070c3ec45d6cb134f3debb2.data)


## Notes:
With this you can add to new and existing scenes: 
- Randomised audio speech or music
- Instant changes to a different collection
- Demo scene included


## Instructions:
- See the guide's description in the Hub


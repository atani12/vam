---
title: Alpha textures guide
date: 2022-12-30
summary: add transparency (or fake rips) to clothes
thumbnail: alphas-guide
mega:
hub: https://hub.virtamate.com/resources/alpha-textures-add-transparency-or-fake-rips-to-clothes.27844/
---

![guide cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/201/201433-e994745c20ba2b5e53b65637759ed7d9.data)


## What are Alpha textures?
Alpha textures are greyscale images that say how transparent things are:

    White (#FFFFFF or 255,255,255) is 0% transparent;
    Black (#000000 or 0,0,0) is 100% transparent;
    Values in between have equivalent transparency.

Transparency in clothing can be used for all kinds of reasons. For example, you can take a t-shirt and add transparency to the lower torso half, now it's a short t-shirt with the stomach showing. You can hide parts you don't want, you can mimic a rip, etc.
Many clothing items come already with alpha textures applied or with presets that use alpha textures to give more options to the clothes. In this tutorial I will show you how you can make your own version.


## Instructions:
- See the guide's description in the Hub

---
title: quickdress presets
date: 2025-01-09
quickdress:
  - atani.quickdress_-_Index.8
  - atani.quickdress_-_Action.8
  - atani.quickdress_-_Barbie.6
  - atani.quickdress_-_Business.8
  - atani.quickdress_-_Cartoons.9
  - atani.quickdress_-_Country.6
  - atani.quickdress_-_DC.6
  - atani.quickdress_-_Disney.1
  - atani.quickdress_-_Fantasy.12
  - atani.quickdress_-_GOT.6
  - atani.quickdress_-_Hogwarts.1
  - atani.quickdress_-_Kate_Bishop.4
  - atani.quickdress_-_Marvel.11
  - atani.quickdress_-_Medical-Military.5
  - atani.quickdress_-_Other_uniforms.7
  - atani.quickdress_-_Schoolgirl.7
  - atani.quickdress_-_Sci-Fi.9
  - atani.quickdress_-_Sport.6
  - atani.quickdress_-_Star_Trek-Wars.3
  - atani.quickdress_-_TV.2
  - atani.quickdress_-_Wanda.4
  - atani.quickdress_-_Wednesday.8
  - atani.quickdress_-_Witcher.8
---

## _quickdress_ Index

This is a special _quickdress_ preset that has a menu to easily load all my currently shared _quickdress_ VARs. It's made purely for loading convenience and hides when the camera focus moves away from it.

Naturally, you can only load _quickdress_ presets with it if you already have the suitable _quickdress_ VAR in your system.

----

**A quickdress VAR includes multiple clothing options.**

## How to use a _quickdress_ preset:
1. Add a **empty atom** and name it _quickdress_ --- my scenes from April 2023 onwards have a _quickdress_ atom already, you don't need to add another
2. Load the empty atom preset from the VAR (_var_:/Custom/Atom/Empty/quickdress/_Preset..._)
3. Click the button to load a outfit
4. Use the buttons to control the clothes

Repeat 2 and 3 to load any _quickdress_ outfit.

### For a second female in a scene:

The VARs now include a _quickdress2_ preset that is meant for a second female atom (Female2) and a _quickdress2_ atom. The procedure is identical apart from the naming as shown in the _requirements_ below.

## Requirements:
- **VaM 1.22.0.3**
- Female atom named **Female** or **Female2** for a second female atom in the scene
- Empty atom named **quickdress** or **quickdress2** for a second female in the scene
- Having the dependencies in the VAR of course

[More information about _quickdress_](https://hub.virtamate.com/resources/quickdress-quickly-load-and-control-clothing-presets.32469/)


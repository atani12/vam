---
title: NEXT
date: 2024-12-09
summary: Modular. Expandable. NEXT
thumbnail: next
images:
  - start
  - ui
  - camera
mega: 
hub: https://hub.virtamate.com/resources/next.49271/
---

![template cover](https://c754e21d1d.mjedge.net/internal_data/attachments/397/397287-f66a5b484d516d61e014bf7db40ecc7c.data)

In this scene you'll find that the persons have built-in skins, no clothes (besides cum clothing), there's no environment, no music. All these you can add yourself in **60 seconds**, but to do that you have to **read the guide** below:

[ How to use the NEXT template scene ](https://atani12.gitlab.io/vam/others/next/)

I'm not joking, in 60 seconds you can add all the missing things very easily, and change to something different just as fast. You can have different themes for this scene without needing to save it, move people around, but you should read the guide to learn how to do that.


### Requirements and notes
- Latest VaM 1.22.0.3
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Optimized Soft Physics setup for more FPS by disabling SP when a female is not "in action" and disabling Glute Physics when the sex action does not need it
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)


### Non-hub assets used:
- None

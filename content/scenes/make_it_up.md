---
title: Make it up
date: 2024-12-09
summary: I'm so sorry, Clint!
thumbnail: make_it_up
images:
  - xmas
  - start-changed-look
  - reset
  - doggy
  - cowgirl
  - stand
mega: 
hub: https://hub.virtamate.com/resources/make-it-up.50080/
---

![banner](https://c754e21d1d.mjedge.net/internal_data/attachments/405/405760-1abfd602a96895512a957a63855f17c0.data)

## About the scene

Kate got cocky, tried to bring down a crime syndicate by herself, and ultimately had to be rescued by Clint. Now Clint missed his flight to get home for Xmas with his family.

She needs to make it up to Clint, somehow...

- rebuilt using [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- 1 intro animation
- cloned voice audio speech for intro and sex actions
- suggested _quickdress_ outfit: [_Kate Bishop_](https://atani12.gitlab.io/vam/clothing/quickdress/)
- apperance preset in _Characters_
- xmas music included
- compatible with _quickplace_ subscenes to easily add a environment; scene's dependency calls for the _quickplace_ Apartment (Nial) for the best results with posing and intro animation, but you can also use any other _quickplace_ for the sex actions

The female starts as **built-in person and nude**. It's preferable to first load a appearance preset and then the quickdress clothes. Afterwards, press the **clean fluids** button to add back the cum clothing.


### Notes:
- Latest VaM
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)


### Non-hub assets used:
- None

----
## v13: 2024-12-09
- updated to NEXT v10

## v12: 2024-11-18
- updated to NEXT v8

## v11: 2024-08-30
- latest NEXT with more sex animations

## v10: 2024-08-22
- updated to the latest NEXT with camera fixes

## v9: 2024-08-19
- Rebuilt with NEXT

## v8: 2023-12-28
- new song plays automatically after the other ends
- groundhog speed increase
- UI clears after clicking a sex position

## v7: 2023-12-18
- fixed UI issues

## v6: 2023-12-18
- updated with the revised 2023 template

## v5: 2023-07-10
- audio speech with cloned voice for intro and sex actions
- small fixes

## v4: 2023-07-10
- Naturalis issues resolved

## v3: 2023-06-17
- reworked main UI
- added facial expressions options
- long hair friendly surfaces to prevent hair sinking

## v2: 2023-05-31
- jingle all the way with Xmas music (Mariah Carey included)

---
title: More than just paper
date: 2022-11-11
summary: Secretary turned saleswoman tries her luck, but it's hard to sell paper in a digital world
thumbnail: more_than_just_paper
images:
  - pam-sit
  - pam-bj
  - pam-desk
  - pam-standing
hub: https://hub.virtamate.com/resources/more-than-just-paper.11792/
---

## About the scene
Based on something you probably saw and named after where the action takes place.


### Notes:
- This is my creation, free to use and adapt
- Audio speech is from a AI text-to-speech synthesizer
- VR or desktop options differences is only about Embody (Passenger) being set as active or not


### Instructions
- Press the VR or desktop button to start

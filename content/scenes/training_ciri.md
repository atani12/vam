---
title: Training Ciri
date: 2023-12-28
summary: Ciri goes deep in Witcher training
thumbnail: training_ciri
images:
  - kiss
  - stand
  - doggy
  - bj
  - cowgirl
mega: https://mega.nz/file/3otzySZK#W7KwJs21-zRByrHT9KGJn4IPglLZJDH3DLq8WRsvUA4
hub: 
---

## About the scene
- this is not my work but a adaptation of [Nicho's *Intimate*](https://hub.virtamate.com/resources/intimate.12572/). The adaptation changes substantially the animations of the original work
- built-in female appearance: use a appearance preset and "calibrate"
- theme music available as multiple parts, use the "next music" button
- two environments available: Tavern (included) & Gwin Cerbin

The female starts as a built-in person. Select a appearance preset of yours and if you want, load a _quickdress_ outfit (see Notes below) or add a clothing preset instead.  While the scene's theme depicts a specific character, you can load anyone else and clothing. Don't forget to press the **add/clean cum clothes** button to add back the cum clothing.


### Notes:
- [quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): load and control a outfit; suggested _quickdress_ preset: **Witcher**
- scales are **1** to make it easier to change appearances
- there will likely be collision issues in cowgirl and bj, possibly other places
- passenger is activated on load when using a VR headset


### Non-hub assets used:
- Tavern environment (included)

----
## v5: 2023-12-28
- new song plays automatically after the other ends

## v4: 2023-12-25
- better lighting in Gwin Cerbin

## v3: 2023-12-25
- new environment: Gwin Cerbin
- some fixes

## v2: 2023-12-24
- Revised and updated to a more flexible setup and playability

---
title: Don't take my phone
date: 2021-12-07
summary: Don't tell my mom, I'll be grounded for a month
thumbnail: dont_take_my_phone
images:
  - katie-shocked
  - katie-undress
  - katie-missionary
  - katie-doggy
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/fssDXACS#p2mCpLjyDFRIYQaUBUp8UYT5MyX_IFf5u5J2ugAoCFc
hub: https://hub.virtamate.com/resources/dont-take-my-phone.12652/
---

## About the scene
_Mega version_: your stepdaughter messed up again

_Hub version_: your tenant is missing on her rent


### Notes:
- This is my creation, free to use and adapt
- Audio speech is from a porn video, don't share it
- 2 endings


### Non-hub assets used:
- None

---
title: nicho's 2+1 templated
date: 2023-09-21
summary: Template of Nicho's 2+1 scene
thumbnail: nicho_2+1_templated
mega: 
hub: https://hub.virtamate.com/resources/nichos-2-1-templated.38965/
---

![template cover](https://1387905758.rsc.cdn77.org/internal_data/attachments/289/289403-e18b91299e8f0500671becc46cb5bfef.data)


## About the scene

This scene is a template adaptation of the fantastic scene from Nicho: [2+1](https://hub.virtamate.com/resources/2-1.8273/). That scene is now over two years old but it's still one of the best scenes ever. If you never saw it, do yourself a favour and play it.

This template creates a easy base to reuse this scene with new environments, clothing, people, and other aspects you may want. You can find further instructions on how to use it in the Hub's page.


### Notes:
- A minimal environment for you to expand with another that is suitable to the animations or use it with VR passthrough (thanks for the tip @Saint66 )​
- Ability to apply different clothes on the females using easily accessible relays​
- All the original scene 5 endings can be followed optionally without ending the scene (multiple orgasms)​
- Cowgirl animations at the end can be skipped to and done repeateadly in any order​
- Fluids used can be changed and are easily reloaded when you change to a different appearance preset​


### Non-hub assets used:
- None


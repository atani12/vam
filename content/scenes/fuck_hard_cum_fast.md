---
title: Fuck Hard, Cum Fast
date: 2023-08-16
summary: No mercy!
thumbnail: fuck_hard_cum_fast
images:
  - fhcf-rest1
  - fhcf-rest2
  - fhcf-bj
  - fhcf-doggy
  - fhcf-table
  - fhcf-groundhog
mega: https://mega.nz/file/zhMnRbbR#hpY1_TYytmFb8fFafgO-WYY64eme-vhKRTZjDum3iEE
hub: 
---

## About the scene
- FFM scene: 3 persons (heavy)
- built from [2023 FFM Template](https://atani12.gitlab.io/vam/scenes/2023_template/)
- Quickdress dependency: [_Sport quickdress_](https://atani12.gitlab.io/vam/clothing/quickdress/) with Gi variants for the theme characters
- females scale is **0.94**
- when you change appearance or clothing press **clean fluids** to merge-load them back

All persons start as built-in persons and nude. Select a appearance preset of yours and if you want, load a _quickdress_ outfit (see Notes below), or add a clothing preset instead. Press the **clean fluids** button to add back the cum clothing.


### Notes:
- **VaM 1.22**
- built-in female appearances: use your own appearance presets
- [_Sport_ quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): has 2 Gis suitable to both characters. See notes on the page
- [quickspeech](https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/): use your own audio assetbundle voices
- quickmusic atom: includes a themed audiobundle with 2 songs
- customisation options and QOL improvements
- passenger is activated on load when using a VR headset
- facial expressions options


### Non-hub assets used:
- None

----

## v3: 2023-08-16
- updated to use the new _quickdress_ Cobra Kai version 

## v2: 2023-07-10
- Naturalis issues resolved

---
title: In a far, far away galaxy
date: 2024-06-20
summary: Into the garbage shoot, fly boy!
thumbnail: in_a_far_far_away_galaxy
images:
  - intro
  - bj
  - table
  - doggy2
  - missionary
  - doggy
mega: 
hub: https://hub.virtamate.com/resources/in-a-far-far-away-galaxy.47863/
---

## About the scene

This scene uses the _2023 template_ and includes **four environments**:
- Starship bedroom
- Jabba's Palace
- Rebel Ship corridor
- Desert tent

The overarching theme should be easy to guess 😉, and together with the environments it also includes multiple music playlists to provide the intended ambience.
This scene is essentially a upgrade and merge of my older scenes - "A boner awakens" and "Laying Leia" - into a single scene that can be used with any character. From those scenes I also include the intro animations.

- built from the latest [2023 Template](https://atani12.gitlab.io/vam/scenes/2023_template/)
- female scale is **1**
- if you change appearance or clothing press **clean fluids** to merge-load them back
- several intro animations
- random themed music playlists
- male appearance presets available (stormtrooper, C-3PO, alien)

The female starts as **built-in person and nude**. You can load a _quickdress_ outfit, I suggest the _Star Trek/Wars quickdress_. Afterwards, press the **clean fluids** button to add back the cum clothing.

This scene may force you to move around a bit depending on the environment in use, especially the Desert tent environment.

### Notes:
- you **need VaM 1.22**
- built-in female appearance: use your own appearance presets
- [quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): load and control outfits with a UI
- [quickspeech](https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/): use your own audio assetbundle voices.
- facial expression options, use "no expressions" if you want to use a plugin for that purpose


### Non-hub assets used:
- none

----
## v1: 2024-06-20
- Hub hosted, renamed and reversioned
- Random music playlists on Soundcloud with hundreds of tracks
- Some optimisations and Hub-friendly dependencies

## v2: 2024-02-06
- enable Naturalis
- table pose tweak

---
title: Karaoke night
date: 2023-09-24
summary: But with less singing and more moaning
thumbnail: karaoke_night
images:
  - push
  - bj
  - tease
  - doggy
  - boobs
  - cowgirl
mega: https://mega.nz/file/f88WWSCZ#eETNmKT1mNIziD3bds8H2gEVMqP69yG9Wx8up7lwQFA
hub: 
---

## About the scene
Press the button to play


### Notes:
- this scene is made using the [*nicho's 2+1 templated*](https://atani12.gitlab.io/vam/scenes/nicho_2+1_templated/) scene
- built-in females used, use your own appearance presets. Make sure to reload cum clothing and the initial scene load clothes in the UI buttons
- there's no music included, but the quickmusic atom has a SoundFromAB logicbrick for assetbundles and the Audiomate plugin for regular audio files. The _play music_ button will trigger a random audio in Audiomate if you include music in its collection
- see the template instructions if you intend to have your own version with different clothing undressing
- renamed from _Modern pussy_ to _Karaoke night_ as the characters used can be any you want. Images in the gallery are from the original scene name theme


### Non-hub assets used:
- CosmicFTW: Clothes_Angel_Set
- JoyBoy: UGGs
- Karaoke room (unknown creator) - included
- Mare_Productions: MP_Flower_Tube_Top
- Mr_CadillacV8: zip_hoodie_a

----

## v3: 2023-09-24
- tweaks in clothing undressing
- animation speed slider

## v2: 2023-09-21
- major structural rework from the template
- renamed from Modern pussy to karaoke night
- smoother animation in multiple areas
- option to change easily appearances

---
title: PhotoStudio
date: 2024-09-25
summary: snip snap
thumbnail: photostudio
images:
  - overview
  - portrait
  - clothes
  - hair
mega: 
hub: https://hub.virtamate.com/resources/photostudio.25301/
---

## About the scene
This is a template scene to take consistent photos for Appearance, Clothes, and Hair presets.

- Pick a mode in the buttons shown
- Click on the Spawn point button to move into a fixed position of the chosen mode
- Load a appearance/clothes/hair preset
- Take your photo

Adapt and save this template scene to fit your needs.


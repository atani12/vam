---
title: 2023 template
date: 2024-03-25
summary: A build-it yourself template for "quick" scene creation
thumbnail: 2023_template
images:
  - main
  - scene
mega: 
hub: https://hub.virtamate.com/resources/2023-template.33120/
---

![template cover](https://c754e21d1d.mjedge.net/internal_data/attachments/395/395685-ddf2980c68832b05a96365268f245962.data)

This scene is a template to "quickly" create scenes with different themes, environments, ambiences, intro mini-scenes, randomised audio speech, quickdress outfits, etc.

## >v23
Ready to play when you use a _quickplace_ subscene available in this website.


## Major breaking changes versions history

This template went through many iterations to improve features, flexibility and organisation. Some of the changes were major breaking changes, others were small QOL updates and fixes. The instructions in this page follows the latest major version but some parts may still use the older versions naming or structure, do adapt accordingly when reading this page.

**Latest major version**: 22
_Previous major version_: 13

- **Latest VaM 1.22.0.3**;
- **Persons at scale 1** for easier appearance changes;
- License is CC BY;
- When you load a appearance preset or clothing preset, press the button "clean fluids" to merge-load cum clothing back on the female;
- UI is compact, prettier (I think so), with improved usability and room for expansion if needed;
- VAMStory Director plugins replaced many logicbricks for increased editing flexibility and managing triggers;
- Sex slaps and Queef sounds;
- Optimized Soft Physics setup for more FPS by disabling SP when a female is not "in action" and disabling Glute Physics when the sex action does not need it;
- Randomization of speed in expressions for a more natural looking result (or sometimes the opposite :LOL:) ;
- Full compatibility with quickdress, quickspeech, quickmusic setups (see my guides);
- The female not "playing" (FFM template) should use a pose to keep FPS as high as possible, but you can of course make a animation for it though, do what you want;
- Facial expression options; use "no expressions" if you want to use a plugin for that purpose or other reasons;
- The scene is biased for a male Passenger VR experience, with little attention given to the male body or POV from other angles.

Read more about this template and how to use it in the Hub's resource page - blue link on the left.


### Non-hub assets used:
- None

---
title: Jurassic Fuck
date: 2024-12-09
summary: You can't tell your mother about this!
thumbnail: jurassic_fuck
images:
  - jf1
  - jf2
  - jf3
  - jf4
  - jf5
  - jf6
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/GkMXlaDA#j0UxUQyNGFW7Djkq33LMW91o-QPUxihrAyrZcVbF8-w
hub: 
---

## About the scene

Intro 1: Claire is a workoholic aunt that nearly had her nephews die in the chaos that enveloped Jurassic World. They kept their mouths quiet about the events, but the older one thinks that his silence has a price.

Intro 2: Claire's nephew is having conscience problems keeping the secret. Unfortunately, the stress from Jurassic World is also affecting his relationship efforts in College. Perhaps Claire can help her nephew deal with these issues?


- rebuilt using [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- 2 intro animations
- cloned voice audio speech for intros and sex actions (excited & disgusted)
- suggested _quickdress_ outfit: [Claire Dearing in _Business_](https://atani12.gitlab.io/vam/clothing/quickdress/)
- apperance preset in _Characters_
- compatible with _quickplace_ subscenes to easily add a environment

The female starts as **built-in person and nude**. It's preferable to first load a appearance preset and then the quickdress clothes. Afterwards, press the **clean fluids** button to add back the cum clothing.


### Notes:
- Latest VaM
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)
- The original room of earlier versions is now a _quickplace_ subscene (restricted), but any other subscene also works

### Non-hub assets used:
- none

----
## v13: 2024-12-09
- updated to NEXT v10

## v12: 2024-11-18
- updated to NEXT v8

## v11: 2024-10-09
- new voiced intro and "excited" sex lines 

## v10: 2024-08-30
- latest NEXT with more sex animations

## v9: 2024-08-22
- updated to the latest NEXT with camera fixes

## v8: 2024-08-19
- rebuilt with NEXT

## v7: 2023-12-28
- groundhog speed increase
- UI clears after clicking a sex position

## v6: 2023-12-26
- updated with the revised 2023 template
- built-in look only

## v5: 2023-07-10
- Naturalis issues resolved

## v4: 2023-06-16
- groundhog position
- reworked main UI
- facial expressions options
- quickspeech audiobundle included in the scene according to the theme (intro and sex actions)

## v3: 2023-05-29
- minor trigger changes

## v2: 2023-05-12
- replace Tittymagic for Naturalis
- minor changes

---
title: The Big Banging
date: 2024-02-01
summary: Penny really needs that wifi password
thumbnail: the_big_banging
images:
  - intro1
  - intro2
  - sex1
  - sex2
  - sex3
  - sex4
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/20U02QyJ#5rYLgDTrKEW-MjRFjRMQTSYfw2-EGFYUgLQ7a_3Zm3M
hub: 
---

## About the scene

- built from the latest [2023 Template](https://atani12.gitlab.io/vam/scenes/2023_template/)
- female scale is **1**
- if you change appearance or clothing press **clean fluids** to merge-load them back
- 4 intro animations
- cloned audio speech for intros and sex actions - excited or comply - based on the chosen expression or intro theme

The female starts as **built-in person and nude**. You can load a _quickdress_ outfit. Afterwards, press the **clean fluids** button to add back the cum clothing.


### Notes:
- you **need VaM 1.22.0.3**
- built-in female appearance: use your own appearance presets
- [quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): load and control outfits with a UI
- [quickspeech](https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/): included on this scene but you can use your own audio assetbundle voices
- quickmusic atom available
- passenger is activated on load when using a VR headset
- facial expression options, use "no expressions" if you want to use a plugin for that purpose


### Non-hub assets used:
- TBBT apartment (included)

----
## v11: 2024-02-01
- updated to the latest template version

## v10: 2023-07-10
- Naturalis issues resolved

## v9: 2023-06-17
- groundhog position
- reworked main UI
- facial expressions options
- quickspeech audiobundle included for intros and sex actions with 2 options: excited or comply (intro related)

## v8: 2023-05-29
- minor trigger changes

## v7: 2023-05-12
- replace Tittymagic for Naturalis
- minor changes

## v6: 16-04-2023
- Redone with the new 2023 template.

## v5: 12-08-2022
- Random speech failsafes and optimisation
- Selectable speech theme: horny or forced
- Some tweaks

## v4 - 09-08-2022
- Some fixes

## v3 - 08-08-2022
- 4 intros
- random speech by theme intro
- male clothes
- likely bugs (WIP) with random speech
- No ambience; use whatever you want

## v2 - 05-08-2022
- Uses the new Scene animation template
- No intros yet

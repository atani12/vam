---
title: cockzilla
date: 2023-07-26
summary: Size does matter :o
thumbnail: cockzilla
images:
  - top
  - side
  - in
  - bulge
  - cumflation
mega: 
hub: https://hub.virtamate.com/resources/cockzilla.36343/
---

## About the scene
- built from [2023 Template](https://atani12.gitlab.io/vam/scenes/2023_template/)
- giants dicks and cumflation theme
- if you change appearance or clothing press **clean fluids** to merge-load them back
- facial expressions options

The female starts as built-in person. Select a appearance preset of yours and if you want, load a _quickdress_ outfit (see Notes below), or add a clothing preset instead. Press the **clean fluids** button to add back the cum clothing.

### Notes:
- you **need VaM 1.22**
- built-in female appearance: use your own appearance presets
- [quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): load and control a outfit; all female clothes in the gallery used _quickdress_ outfits
- [quickspeech](https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/): use your own audio assetbundle voices.
- quickmusic atom: add a audio assetbundle if you have any or add Audiomate in it and update the main UI buttons (blue) triggers to control it from there
- customisation options and QOL improvements
- passenger is activated on load when using a VR headset
- facial expression options, use "no expressions" if you want to use a plugin for that purpose


### Non-hub assets used:
- None

----

## v6-9: 2023-07-26
- penis morph preset
- queefing and cumflation sounds
- multiple fixes and optimisations

## v5: 2023-07-10
- doggy position
- options to move closer
- other tweaks

## v5: 2023-07-10
- naturalis issues resolved

## v4: 2023-07-09
- new UI options and cum hose animation

## v3: 2023-07-08
- more cum options and tweaks

## v2: 2023-07-04
- simplification and optimisations

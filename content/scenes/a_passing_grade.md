---
title: A passing grade
date: 2024-11-21
summary: Earn those extra points
thumbnail: a_passing_grade
images:
  - teacher
  - student
  - bj
  - intro
  - doggy
  - missionary
  - table
mega: 
hub: https://hub.virtamate.com/resources/a-passing-grade.21491/
---

![banner](https://1387905758.rsc.cdn77.org/internal_data/attachments/268/268988-d301e9b16db5d000f93ea8d41f985b07.data)


## About the scene
Play as a teacher or student who is offered a unique opportunity.

- rebuilt using [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- 5 intro animations
- multiple audio speech for intros and sex actions
- compatible with _quickplace_ subscenes to easily add a environment; scene's dependency calls for the _quickplace_ Classroom (VaMSleSar) for the best results with posing and intro animations, but you can also use any other _quickplaces_ for the sex actions

The female starts as **built-in person and nude**. It's preferable to first load a appearance preset and then the quickdress clothes. Afterwards, press the **clean fluids** button to add back the cum clothing.


### Notes:
- Latest VaM
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)


### Non-hub assets used:
- None

----
## v10: 2024-09-24
- updated to the latest NEXT

## v9: 2024-04-12
- migrated to the latest 2023 Template version
- various improvements

## v7: 2023-07-14
- major overaul to use the 2023 Template
- audio speech for intros and sex actions 

## v6: 31-07-2022
- Pervy mode adds students in the classroom, when before they were only shown with intro 4

## v5: 31-07-2022
- Resolved problem with Timeline segment order when starting the scene and clicking BJ

## v4: 30-07-2022
- The hub version has small fixes and will be the only one available, I won't maintain 2 versions
- Included environment and payed assets are changed to Hub free assets

## v3: 28-07-2022
- Cum fluids screwup is fixed
- Small fixes and tweaks

## v2: 28-07-2022
- New teacher intro - Sex class
- Small fixes

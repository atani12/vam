---
title: Perilous Woods
date: 2025-01-06
summary: It's dangerous to go alone!
thumbnail: perilous_woods
images:
  - elf
  - red1
  - red2
  - pose
  - sex1
  - sex2
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/jt912ZbJ#G06Hk2QCDe-oRj8_IcDaH3DcFLb5_FVsIY-cAu9Pqbo
hub: 
---

## About the scene

This is a fantasy themed scene inspired by the artwork of [Ninjartist](http://img0.thatpervert.com/pics/post/full/lord-of-the-rings--Arda-fandoms-5656108.jpeg) and [Ruthven's scenes](https://hub.virtamate.com/resources/little-red.7107/).
The "monster" based scene has the male at a larger scale than the female, suited for example for Orcs, Trolls, Werewolves, etc.

- rebuilt using [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- 2 intro animations
- forest environment dependency is a _quickplace_ subscene, but others will also work
- male is at scale 1.25
- [fantasy males presets available](https://atani12.gitlab.io/vam/others/fantasy_males/)

The persons start as **built-in persons and nude**. It's preferable to first load a appearance preset and then any quickdress clothes. Afterwards, press the **clean fluids** button to add back the cum clothing.

### Notes:
- Latest VaM
- Female at scale **1** and male at **1.25**
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)

### Non-hub assets used:
- [_quickplace_ Forest day/night](https://atani12.gitlab.io/vam/quickplace/hub/)

----
## v6: 2025-01-06
- added cave environment button
- animation male head position tweaks

## v5: 2025-01-01
- reworked to NEXT v10

## v4: 2024-05-26
- added missing reset triggers for scale 1.3 extra positions UI

## v3: 2024-05-18
- merged "the Troll" scene (removed) with this one as the theme is similar
- improvements overall

## v2: 2024-01-25
- fixed problems with male presets
- removed scene plugin

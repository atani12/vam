---
title: The Cock of the Rings
date: 2023-09-24
summary: The One Cock to fuck them all
thumbnail: the_cock_of_the_rings
images:
  - arwen-doggy
  - arwen-cowgirl
  - arwen-tauriel-bj
  - arwen-tauriel-kiss
  - arwen-tauriel-tease
  - tauriel-cowgirl
mega: https://mega.nz/file/CwFhmS6a#9Qw9nUuZFUZWiOenpbhiZJ4uwKKZx54VxpdznHALzYA
hub: 
---

## About the scene
Press the button to play


### Notes:
- this scene is made using the [*nicho's 2+1 templated*](https://atani12.gitlab.io/vam/scenes/nicho_2+1_templated/) scene
- 2 themed music options with multiple parts each; press the button to jump to another track
- see the template instructions if you intend to have your own version with different characters and clothing undressing
- former name of this scene was _Lothlorgia_, but wasn't too happy with the name


### Non-hub assets used:
- Atani: Pubes-Makeup (link in homepage)
- Bamair1984: Liv_Tyler
- DIO: Hypnotic
- vecterror: EvLi23

----

## v3: 2023-09-24
- small tweaks in animation on bj
- volume and animation speed slider

## v2: 2023-09-21
- major structural rework from the template
- renamed from Lothlorgia to The Cock of the Rings
- music options
- smoother animation in multiple areas

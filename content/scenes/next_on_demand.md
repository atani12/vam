---
title: NEXT on demand
date: 2024-08-21
summary: Build and play when you want it
thumbnail: next_on_demand
images:
  - erica-bj
  - erica-doggy
  - pc-rest
  - pc-missionary
  - wtd_sex2
  - wtd_sex3
mega: 
hub: 
---

## Scenes on demand

Some older scenes I made and updated over time can now be recreated on demand by combining existing resources like _quickdress_, _quickplace_, _quickmusic_, _quickspeech_ and _Characters_ appearance presets, applied to the NEXT template.

The gallery shows some pictures I took for those older scenes, just to give some ideas on what can be done quickly. Naturally, (pseudo)artistic tweaks were made for taking the pictures, so don't treat them as entirelly recreatable.

### How to make a simple scene from NEXT:

- [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- Stuff I share on this website or you have around
- A minute to put it all together

**When loading a appearance and clothing presets, press the _clean fluids_ button to add back the cum clothing.**

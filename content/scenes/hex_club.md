---
title: Hex club
date: 2023-08-03
summary: Cum and join us
thumbnail: hex_club
images:
  - discovery
  - approach
  - join
tag: ⚠️ sensitive themes
mega: 
hub: https://hub.virtamate.com/resources/hex-club.37508/
---

## About the scene

This scene has themes that some people find uncomfortable. In this scene expect:
- Magic, spells, hexes, and strange entities​
- Transgenderism​
- Potential stressful moments​

If you are easily disturbed by the themes above, then please move along and ignore this scene. Reports or low ratings because you don't like the themes is not a acceptable justification. However, if you do have some concern about the scene's tone, please do mention it in the Discussion area of this resource and I'll reply back.

For everyone else that wants to try the experience, download it and have fun.

### Notes:
- Click the button play in Desktop (Camera Ride) or VR Passenger
- Likely suitable for pre-1.21 VaM versions 

### Non-hub assets used:
- None


---
title: A Bust gone bust
date: 2023-01-12
summary: Don't let your guard down!
thumbnail: a_bust_gone_bust
images:
  - undercover
  - frame
  - surprise
  - bust
hub: https://hub.virtamate.com/resources/a-bust-gone-bust.19671/
---

### Notes:
- This is my creation, free to use and adapt


### Instructions
It's on autoplay and soon a dialog will pop up. Click on it to proceed.

**Dialogs guide**:
- White background: click to move the animation along
- Grey background: no action needed, disappears after a few seconds

### Non-hub assets used:
- None

---
title: Summers fun
date: 2021-10-25
summary: The Summers (and friends) left the TV world to join the VaM universe
thumbnail: summers_fun
images:
  - buffy-catwalk
  - buffy-punch
  - buffy-bj
  - buffy-undress
  - willow-spell
  - willow-bj
  - dawn-shock
  - dawn-invite
  - dawn-doggy
mega: https://mega.nz/file/agd3HA6Y#VGtCgfGG3rK8TrKnHdw641ybmQYuT798miznXpWgzAI
hub: https://hub.virtamate.com/resources/summer-s-fun.11296/
---

## About the scene
This is my creation, free to use and adapt

### Notes:
- 2 scenes
- Library scene has 2 endings
- Buffy's appearance was done with version 1 of Boyavam's Muffy
- This version is different from the Hub's version in the female appearance used in the Breaking Dawn scene.


### Non-hub assets used:
- Zombie_Siris: Shelly_T_Burger

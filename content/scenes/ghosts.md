---
title: Ghosts
date: 2025-02-14
summary: Spirit encounters of the thrusting kind
thumbnail: ghosts
images:
  - front
  - side
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/ikkEFQoL#zZRRpvfkoUDx6gG9-0lSqMN8jN_CrPbKaLNQGh3cxWM
hub: 
---

## About the scene
Your uneasy soul has been stuck for centuries in a limbo, unable to move on. But existing as an immaterial being does have its rewards when interacting with the living.

The scene is made from NEXT, starts with a brief intro, and is limited in sex acts as the female atom is sleeping. The male is a ghost if it wasn't clear yet. The female starts as **built-in person and nude**.

Click the "reposition environment" button when you load a _quickplace_ environment. There's no garantee that a bed is used for the available positions, which would make the most sense for the scene's theme. As the missionary position is most likely to use one, that's the position I force in the environment.


### Requirements and notes
- Latest VaM
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- 2 positions only as the others don't suit well the scene's theme
- Full compatibility with quickplace, quickdress, quickmusic setups (see the guide)


### Non-hub assets used:
- [Ghost penetrator](https://www.patreon.com/posts/54808339) (free)

----
## v4: 2025-02-14
- updated to NEXT v10
- improvement on animations and functionalities

## v3: 2024-09-09
- updated to latest NEXT 

## v2: 2024-08-06
- quick fixes

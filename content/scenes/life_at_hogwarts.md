---
title: Life at Hogwarts
date: 2024-12-12
summary: Petrificus Totalus Dickus
thumbnail: life_at_hogwarts
images:
  - life_at_hogwarts-dorm
  - life_at_hogwarts-dorm-miss
  - life_at_hogwarts-dorm-bj
  - vw-intro1
  - vw-intro2
  - vw-sex1
  - vw-sex
  - vw-sex2
tag: ⚠️ sensitive themes
mega: https://mega.nz/file/istyyC7a#rnppa9C-YzLBqKGBrJdw0lPI0HAsoKqWxFLgTQyIAHQ
hub: 
---

## About the scene

This is a multiple environments Harry Potter themed scene:

- built from the latest [NEXT Template](https://atani12.gitlab.io/vam/scenes/next/)
- intro animation
- ambient music
- audio speech in intro and sex animations


### Notes:
- Latest VaM
- Persons at scale 1 for easier appearance changes
- When you load a appearance preset or clothing preset, press the button "add/clean fluids" to merge-load cum clothing back on the female
- Full compatibility with quickplace, quickdress, quickspeech, quickmusic setups (see the guide)


### Non-hub assets used:
- [Harry Potter _quickmusic_](https://atani12.gitlab.io/vam/others/quickmusic/)
- [Hogwarts _quickdress_](https://atani12.gitlab.io/vam/clothing/quickdress/)
- [Old bedroom and fireplace room _quickplaces_](https://atani12.gitlab.io/vam/quickplace/hub/)

----
## v11: 2024-12-12
- updated to NEXT v10

## v10: 2024-11-23
- updated to the latest NEXT
- mulltiple environments
- selectable Voldemort character or generic robe
- adaptable and selectable audio speech options (female)

## v9: 2024-09-20
- rebuilt with NEXT

## v8: 2024-07-06
- fix minor issues

## v7: 2024-01-30
- upgraded to the latest _2023 template_
- removed "Gryffinwhoring" and "Butterbeer with Hagrid": merged some features into existing scenes

## v6: 2023-08-14
- new scene "Gryffinwhoring": a continuation of "Voldemort wins" based on the "cockzilla" scene

## v5: 2023-07-10
- Naturalis issues resolved

## v4: 2023-06-20
- groundhog position in Gryfindor Chambers
- reworked main UI
- facial expressions options
- quickspeech audiobundle included for intros and _Butterbeer with Hagrid_ sex actions

## v3: 2023-05-29
- minor trigger changes
- replace Tittymagic for Naturalis

## v2: 2023-05-16
- Fix some mistakes and small QOL improvements


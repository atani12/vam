---
title: Nevermore Academy
date: 2023-12-29
summary: Welcoming Sex Outcasts of all kinks for over 200 years
thumbnail: nevermore_academy
images:
  - na - rest
  - na - bj
  - na - doggy
  - na - doggybed
  - na - table
  - na - missionary
mega: 
hub: https://hub.virtamate.com/resources/nevermore-academy.35953/
---

## About the scene
- built from the revised [2023 Template](https://atani12.gitlab.io/vam/scenes/2023_template/)
- FFM scene: 3 persons (heavy)
- female scale is **1** :D
- if you change appearance or clothing press **clean fluids** to merge-load them back
- facial expressions options
- themed music included
- there's a suggested _quickdress_ outfit: [Wednesday](https://atani12.gitlab.io/vam/img/clothing/quickdress/); you can also used others of course

The females starts as **built-in person and nude**. I share appearance presets that are within the theme as well as a _quickdress_ outfit, but feel free to use your own. It's preferable to first load a appearance preset and then the quickdress clothes. Afterwards, press the **clean fluids** button to add back the cum clothing.


### Notes:
- **you need VaM 1.22**
- built-in female appearances: use a appearance preset
- [quickdress](https://atani12.gitlab.io/vam/clothing/quickdress/): load and control a outfit, suggested _quickdress_ preset: **Wednesday**
- [quickspeech](https://hub.virtamate.com/resources/quickspeech-quickly-load-and-switch-random-audio-speech.30482/): use your own audio assetbundle voices
- quickmusic atom: includes a themed audiobundle
- passenger is activated on load when using a VR headset
- facial expression options, use "no expressions" if you want to use a plugin for that purpose


### Non-hub assets used:
- None

----
## v9: 2023-12-29
- Missing triggers in Table(2) position for variations in speed
- Adjusted female heads positions in Table(2) to prevent headbutting the male and with small variations between them

## v8: 2023-12-28
- updated with the revised 2023 template
- new environment that saves 10-15 FPS compared to the previously used CUA
- Thing joined in for a foursome... actually more like a 3.1 person act

## v6: 2023-07-10
- Naturalis issues resolved

## v4: 2023-06-30
- gaze and auto-expressions when in pose during the other female's sex action

## v3: 2023-06-24
- hub hosted
- up to date with the latest FFM template functionalities
- reworked main UI
- facial expressions options
- themed ambient music (_quickmusic_ atom)

## v2: 2023-05-30
- small tweaks and fixes
- new buttons in OptionsUI to control Naturalis soft body physics (FPS)

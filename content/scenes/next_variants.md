---
title: NEXT variants
date: 2024-12-09
summary: Modified scenes of the NEXT template for other uses
thumbnail: next_variants
images:
  - next_sex_doll_variant
  - next_alt_futa_variant
mega: 
hub: 
---

![template cover](https://c754e21d1d.mjedge.net/internal_data/attachments/397/397292-0bea1e92efb3b32a488ea689c35a9178.data)

These variants are altered versions of the [NEXT template scene](https://atani12.gitlab.io/vam/scenes/next/) for specific themes or uses.

As these scenes are made from NEXT, almost or all features of the original template are available. Highly suggested is to use the _quickplace_ subscenes.

### Variant: sex doll

[Scene](https://hub.virtamate.com/resources/next-variants-sex-doll.48845/)

The female atom here acts as a sex doll. All expressions from the female were removed, no blinking, no moaning, no movement. It's now a hyper-realistic inert sex doll.

[Available doll boxes](https://atani12.gitlab.io/vam/others/doll_boxes/)

### Variant: alt futa

[Scene](https://hub.virtamate.com/resources/next-variants-alt-futa.50370/)

The male atom was replaced with a female atom that is using the "alt futa" plugin from Stopper. This means that "Female2" has a penis, no female genitalia, but everything else is female.

---
title: Action
date: 2024-11-14
app_folder: Action
characters:
  - name: Buffy
    image: Preset_Buffy
    quickdress: atani.quickdress_-_Action.8
  - name: "Cobra Kai: Sam"
    image: Preset_Cobra Kai - Sam LaRusso
    quickdress: atani.quickdress_-_Sport.6
  - name: "Cobra Kai: Tory"
    image: Preset_Cobra Kai - Tory Nichols
    quickdress: atani.quickdress_-_Sport.6
  - name: Jumanji
    image: Preset_Jumanji
    quickdress: atani.quickdress_-_Action.8
  - name: "Kick-Ass: Hit Girl"
    image: Preset_Hit Girl
    quickdress: atani.quickdress_-_Action.8
  - name: Kill Bill
    image: Preset_Kill Bill
    quickdress: atani.quickdress_-_Action.8
  - name: Lara Croft
    image: Preset_Lara Croft
    quickdress: atani.quickdress_-_Action.8
  - name: "Lost: Kate"
    image: Preset_Kate
    quickdress: atani.quickdress_-_Action.8
  - name: SuckerPunch
    image: Preset_SuckerPunch
    quickdress: atani.quickdress_-_Action.8
  - name: "The Boys: Kimiko"
    image: Preset_The Boys - Kimiko
    quickdress: atani.quickdress_-_Action.8
  - name: "The Boys: Queen Maeve"
    image: Preset_The Boys - Queen Maeve
    quickdress: atani.quickdress_-_Action.8
  - name: "The Boys: Starlight"
    image: Preset_The Boys - Starlight
    quickdress: atani.quickdress_-_Action.8
  - name: "The Hunger Games: Katniss"
    image: Preset_Katniss Everdeen
    quickdress: atani.quickdress_-_Action.8
  - name: "Vikings: Lagertha"
    image: Preset_Lagertha
    quickdress: atani.quickdress_-_Action.8
  - name: Xena
    image: Preset_Xena
    quickdress: atani.quickdress_-_Action.8
---



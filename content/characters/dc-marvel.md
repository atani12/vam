---
title: DC & Marvel
date: 2024-10-05
app_folder: DC-Marvel
characters:
  - name: Black Widow
    image: Preset_Black Widow
    quickdress: atani.quickdress_-_Marvel.11
  - name: Black Widow
    image: Preset_Black Widow 2
    quickdress: atani.quickdress_-_Marvel.11
  - name: Captain Carter
    image: Preset_Captain Carter
    quickdress: atani.quickdress_-_Marvel.11
  - name: Captain Marvel
    image: Preset_Captain Marvel
    quickdress: atani.quickdress_-_Marvel.11
  - name: Catwoman
    image: Preset_Catwoman
    quickdress: atani.quickdress_-_DC.6
  - name: Daisy Johnson
    image: Preset_Daisy Johnson
    quickdress: atani.quickdress_-_Marvel.11
  - name: Gamorra
    image: Preset_Gamorra
    quickdress: atani.quickdress_-_Marvel.11
  - name: Harley Quinn
    image: Preset_Harley Quinn
    quickdress: atani.quickdress_-_Marvel.11
  - name: Jessica Jones
    image: Preset_Jessica Jones
    quickdress: atani.quickdress_-_DC.6
  - name: Kate Bishop
    image: Preset_Kate Bishop
    quickdress: atani.quickdress_-_Kate_Bishop.4
  - name: Kate Bishop
    image: Preset_Kate Bishop 2
    quickdress: atani.quickdress_-_Kate_Bishop.4
  - name: Lady Sif
    image: Preset_Lady Sif
    quickdress: atani.quickdress_-_Marvel.11
  - name: Mantis
    image: Preset_Mantis
    quickdress: atani.quickdress_-_Marvel.11
  - name: Mera
    image: Preset_Mera
    quickdress: atani.quickdress_-_Marvel.11
  - name: Ms. Marvel
    image: Preset_Ms. Marvel
    quickdress: atani.quickdress_-_Marvel.11
  - name: Mystique
    image: Preset_Mystique
    quickdress: 
  - name: Peggy Carter
    image: Preset_Peggy Carter
    quickdress: atani.quickdress_-_Medical-Military.5
  - name: She-Hulk
    image: Preset_She-Hulk
    quickdress: atani.quickdress_-_Business.8
  - name: She-Hulk
    image: Preset_She-Hulk 2
    quickdress: atani.quickdress_-_Business.8
  - name: Supergirl
    image: Preset_Supergirl
    quickdress: atani.quickdress_-_Marvel.11
  - name: Supergirl
    image: Preset_Supergirl 2
    quickdress: atani.quickdress_-_Marvel.11
  - name: Wanda
    image: Preset_Wanda
    quickdress: atani.quickdress_-_Wanda.4
  - name: Wonder Woman
    image: Preset_Wonder Woman
    quickdress: atani.quickdress_-_DC.6
  - name: Wonder Woman
    image: Preset_Wonder Woman 2
    quickdress: atani.quickdress_-_DC.6
---



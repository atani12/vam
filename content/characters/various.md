---
title: Various
date: 2025-02-22
app_folder: Various
characters:
  - name: "Back to the Future: Lorraine"
    image: Preset_Back to the Future - Lorraine McFly
    quickdress: atani.quickdress_-_Wanda.4
  - name: Barbie
    image: Preset_Barbie
    quickdress: atani.quickdress_-_Barbie.6
  - name: "Baywatch: CJ"
    image: Preset_Baywatch - CJ
    quickdress: atani.quickdress_-_Sport.6
  - name: "Brooklynn 99: Amy Santiago"
    image: Preset_Brooklynn 99 - Amy Santiago
    quickdress: atani.quickdress_-_TV.2
  - name: "Brooklynn 99: Rosa Diaz"
    image: Preset_Brooklynn 99 - Rosa Diaz
    quickdress: atani.quickdress_-_TV.2
  - name: "Jurassic World: Claire Dearing"
    image: Preset_Jurassic World - Claire Dearing
    quickdress: atani.quickdress_-_Business.8
  - name: "Malcolm in the middle: Lois"
    image: Preset_Malcolm in the middle - Lois
    quickdress: 
  - name: "Married with children: Kelly Bundy"
    image: Preset_Married with Children - Kelly Bundy
    quickdress: atani.quickdress_-_TV.2
  - name: "Married with children: Peggy Bundy"
    image: Preset_Married with Children - Peggy Bundy
    quickdress: atani.quickdress_-_TV.2
  - name: "Saved by the bell: Kelly Kapowsky"
    image: Preset_Saved by the bell - Kelly Kapowsky
    quickdress: atani.quickdress_-_TV.2
  - name: "Seinfeld: Elaine"
    image: Preset_Seinfeld - Elaine Benes
    quickdress: atani.quickdress_-_TV.2
  - name: "Shogun: Mariko"
    image: Preset_Shogun - Mariko
    quickdress: 
  - name: "Titanic: Rose"
    image: Preset_Titanic - Rose
    quickdress: 
  - name: "The X-Files: Dana Scully"
    image: Preset_X-Files - Dana Scully
    quickdress: atani.quickdress_-_Business.8
  - name: "The X-Files: Dana Scully"
    image: Preset_X-Files - Dana Scully 2
    quickdress: atani.quickdress_-_Business.8
  - name: Veep
    image: Preset_Veep
    quickdress: atani.quickdress_-_TV.2
---



---
layout: layouts/home.njk
---
<section class="hometext">

<article>

# TL/DR:

- This website **only offers preset files (vap)<sup>1</sup>** and links to the Hub and/or Mega for content made by me
- **Provided as-is**: dependencies are your responsibility
- If a link is broken please let me know about it, thanks
- All content shown and shared on this website includes only adults in shape, story or theme, and is **for adult use only**. If you are not a adult yet, piss off 🖕 and go play Fortnite or whatever

**<sup>1</sup>** Presets depend on you having the referenced assets. You can open the vap file in Notepad to see what it needs. A few presets might refer to packages with my creator name (atani) that may not be released anywhere. Read more about it in the section "Missing dependencies" below.
</article>

<article>

#### Latest website and content announcements:

- 11-11-2024:
  - rework and merge of _quickplace_ types into a single type
- 11-08-2024:
  - titled _quickplace_ subscenes and new "restricted" category
- 10-08-2024:
  - dependencies for presets available as a txt file 
- 18-04-2024:
  - combined _quickplace_ subscenes for easier management and use
- 24-03-2024:
  - a new type of resource: _quickplace_ subscenes

</article>

<article>

## Missing dependencies

As noted above, you need to get the dependencies yourself. They may be free in the Hub, or could be payed from Patreon, and some might be even not available anywhere.

### atani dependencies

These are mainly repacks of pre-VAR content from other creators. Find them in [Bunkr](https://bunkr.sk/a/O21mmNET)

### Preset VAR checker

I made a [python script](https://atani12.gitlab.io/vam/others/preset_checker/) that checks your presets and VARs and then lists what you're missing. This is very useful if you download the presets I'm sharing as you can use the list to more quickly search online for the missing VARs.

</article>

<article>

## About this site

I'm sharing a bunch of things I made which I hope is of interest to you. Some of the shared scenes are of my creation and others are scene adaptations from other creators. Relevant information is shown on the resource's page.

All resources shared here may or may not reference non-hub assets. I may list them, but I do not share them, you need to get them yourself. I try to follow the license terms in shared assets but if you see something that shouldn't be shared do let me know (contact below).

This content is meant for personal enjoyment, feel free to adapt it in any way you want. Don't share these on the Hub, especially not the adaptations, unless it's a original of mine and the license allows it.

### Contact

If you want to contact me for any matter this is [my profile on the HUB](https://hub.virtamate.com/members/atani.6611/) or find me with the handle **atani** in VaM related Discord servers.

</article>

</section>
---
layout: layouts/home.njk
eleventyNavigation:
  key: quickplace
  order: 5
---
<h1><i>quickplace</i></h1>

{% set postslist = collections.quickplace %}
{% include "categorylist.njk" %}

<section class="hometext">
<article>

## What are _quickplace_ subscenes?

They are subscenes with environments that you can quickly load in a compatible scene.

The goal is to have a modular and scalable approach to multiple environments in compatible scenes. This allows you to **reuse a scene** with a different experience without having to create multiple copies or include the environments in the scene.

## How to use a _quickplace_ subscene:

1. Find the subscene atom _quickplace_ in the scene
2. Load the _quickplace_ VAR you downloaded in the subscene atom

### Scenes compatible with the shared _quickplace_ subscenes:

- Public access:
  - [NEXT](https://atani12.gitlab.io/vam/scenes/next/)
- Restricted access: ask me at a certain place
</article>

<article>

## Types of _quickplace_ subscenes

There's now a single type of _quickplace_ subscene, the Template.

Timeline is used to reposition the environment automatically. This is accomplished with the same-named segments/animations sync feature of both the persons Timeline instances and on the environment's Timeline plugin.

Earlier there were other types, "flat" and "sit", which were separate setups for other creators scenes with such poses. The current NEXT (v7>) pose options and updated _quickplace_ subscenes allowed merging these types into the single "template" type. 


## How to make _quickplace_ subscenes

[quickplace guide](https://hub.virtamate.com/resources/quickplace-subscenes.45008/)

[quickplace extended guide](https://hub.virtamate.com/resources/quickplace-subscenes-extended.45086/)

</article>
</section>
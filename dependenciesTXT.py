#!/usr/bin/python3
import time
import json
import re
from pathlib import Path

# Preset VAR checker
# lists dependencies in presets and creates a dependencies txt file

appfolder = Path("./img/characters")
clothfolder = Path("./img/clothing/presets")

# List for appearance presets

print("Creating txt for appearance presets")
for files in appfolder.rglob("*.vap"):
    vap = open(files, "r", encoding="utf8")
    dataApp = json.load(vap)
    varnamelistAppearances = []
    clothing = dataApp["storables"][0]["clothing"]
    hair = dataApp["storables"][0]["hair"]
    morph = dataApp["storables"][0]["morphs"]
    for item in clothing:
        if ":/" not in item["id"]:
            continue
        else:
            varnamesplit = re.split(r":",item["id"]).pop(0)
            varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
            varnamelistAppearances.append(varnamesplitagain)
    for item in hair:
        if ":/" not in item["id"]:
            continue
        else:
            varnamesplit = re.split(r":",item["id"]).pop(0)
            varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
            varnamelistAppearances.append(varnamesplitagain)
    for item in morph:
        if ":/" not in item["uid"]:
            continue
        else:
            varnamesplit = re.split(r":",item["uid"]).pop(0)
            varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
            varnamelistAppearances.append(varnamesplitagain)
    # remainder objects in appearance preset
    objAppearance = dataApp["storables"]
    for item in objAppearance:
        for value in item.values():
            if ":/" in value:
                varnamesplit = re.split(r":",value).pop(0)
                varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
                varnamelistAppearances.append(varnamesplitagain)
            else:
                continue
    varnameAppearances = sorted(list(set(varnamelistAppearances)), key=str.casefold)
    txtfile = str(files) + ".txt"
    with open (txtfile ,'w') as f:
        for item in varnameAppearances:
            f.write(f"{item}\n")
    f.close()


# List for clothing presets

print("Creating txt for clothing presets")
for files in clothfolder.rglob("*.vap"):
    vap = open(files, "r", encoding="utf8")
    dataClothing = json.load(vap)
    varnamelistClothing = []
    clothing = dataClothing["storables"][0]["clothing"]
    for item in clothing:
        if ":/" not in item["id"]:
            continue
        else:
            varnamesplit = re.split(r":",item["id"]).pop(0)
            varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
            varnamelistClothing.append(varnamesplitagain)
    objClothing = dataClothing["storables"]
    for item in objClothing:
        for value in item.values():
            if ":/" in value:
                varnamesplit = re.split(r":",value).pop(0)
                varnamesplitagain = varnamesplit.rsplit(".", 1).pop(0)
                varnamelistClothing.append(varnamesplitagain)
            else:
                continue
    varnameClothing = sorted(list(set(varnamelistClothing)), key=str.casefold)
    txtfile = str(files) + ".txt"
    with open (txtfile ,'w') as f:
        for item in varnameClothing:
            f.write(f"{item}\n")
    f.close()

print("All done!")